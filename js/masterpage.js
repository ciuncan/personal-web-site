
var MENU_ITEMS_SELECTOR  = "nav li";

$(function() {
  $(MENU_ITEMS_SELECTOR).click(function () {
    //event target is the anchor tag inside the menu items of navigation bar.
    //its parent will give menu item
    var pageName = getNameFromId($(this)[0].id);
    setTab(pageName);
    loadContentFromPage(pageName + '.html');
  })[0].click();
});

function queryObj() {
  var result = {}, keyValuePairs = location.search.slice(1).split('&');

  if (keyValuePairs.length === 0) {
    return []; 
  }
  keyValuePairs.forEach(function(keyValuePair) {
    keyValuePair = keyValuePair.split('=');
    result[keyValuePair[0]] = keyValuePair[1] || '';
  });

  return result;
}

function setTab(tabName) {
  $(MENU_ITEMS_SELECTOR).removeClass("current");
  $("#" + tabName + '_element').addClass('current');
}

function loadContentFromPage(url, postLoadFun) {
  $("#content")
    .html('<h1>Loading...</h1>') 
    .load(url, 
      function(response, status, xhr) {
        if (status === "error") {
          $(this).html('<p>Connection failed.<br/>' + 
                       '<a href="javascript: loadContentFromPage(' + url + ',' + 
                         postLoadFun + ');">Try again?</a></p>');
        } else {
          $(this).ready(postLoadFun);
        }
      } 
    );
}

function loadContentPretty(url) {
  loadContentFromPage(url, function() { prettyPrint(); });
}

function getNameFromId(id) {
  return id.substring(0, id.indexOf('_'));
}

function setCourse(semester, course) {
  setTab('courses');
  loadContentFromPage('courses/' + semester + '/' + course + '/index.html');
}

